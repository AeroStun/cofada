#include "Personne.h"

Personne::Personne(std::string prenom, int nbParticipants) : m_prenom(prenom), m_depensesTotales(0.0f), m_operationsBTotales(0.0f){
	m_dettesCreances.resize(nbParticipants);
}

std::string Personne::getPrenom() const {
	return m_prenom;
}

double Personne::getDepensesTotales() const {
	return m_depensesTotales;
}

void Personne::setDetteCreance(int indicePrenom, double montant){
	m_dettesCreances[indicePrenom].setDetteCreance(montant);
}

OperationB Personne::getDetteCreance(int indicePrenom) const {
	return m_dettesCreances[indicePrenom];
}

double Personne::getOperationsBTotales() const {
	return m_operationsBTotales;
}

void Personne::ajoutDepense(double montantDepense){
	m_depensesTotales += montantDepense;
}

void Personne::calculOperationsBTotales(){
	double total = 0.0f;
	for(const auto& v : m_dettesCreances)
		total += v.getDetteCreance();
	m_operationsBTotales = total;
}
