#include <iostream>
#include "Compte.h"

Compte::Compte(std::string nom, std::string dateDebut, std::string dateFin, int nbParticipants) : m_nom(nom), m_dateDebut(dateDebut), m_dateFin(dateFin), m_budgetTotalDepense(0.0), m_partIndividuelle(0.0){
	std::string s;
	for(int i = 0; i < nbParticipants; ++i){
		std::cout << "Prénom du participant : " << std::flush;
		std::cin >> s;
		m_participants.emplace_back(s, nbParticipants);
	}
	std::cout << "Compte " << m_nom << " créé !" << std::endl;
}

std::size_t Compte::getNbParticipants() const {
	return m_participants.size();
}

double Compte::getBudgetTotalDepense() const {
	return m_budgetTotalDepense;
}

double Compte::getPartIndividuelle() const {
	return m_partIndividuelle;
}

Personne Compte::getParticipant(int indicePrenom) const {
	return m_participants[indicePrenom];
}

const std::vector<Personne>& Compte::getParticipants() const {
	return m_participants;
}

void Compte::ajoutDepenseBudget(int indicePrenom, double nouvelleDepense){
	// ajout de la dépense
	m_participants[indicePrenom].ajoutDepense(nouvelleDepense);

	// calcul des nouvelles dettes
	double dette;
	for(auto j = 0u; j < getNbParticipants(); ++j){
		for(auto i = 0u; i < getNbParticipants(); ++i){
			// calcul de la dette de indicePrenom à i
			dette = (m_participants[i].getDepensesTotales() - m_participants[j].getDepensesTotales()) / getNbParticipants(); // The comment doesn't help at all
			// This double loop is probably not the best way of achieveing your goal
			// mise à jour de indicePrenom::dettesCreances pour i
			m_participants[j].setDetteCreance(i, dette);
		}
		m_participants[j].calculOperationsBTotales();
	}

	// mise à jour compte
	m_budgetTotalDepense += nouvelleDepense;
	m_partIndividuelle = m_budgetTotalDepense / getNbParticipants();
}
