#pragma once

class OperationB {
public:

	// Accesseurs
	double getDetteCreance() const;
	void setDetteCreance(double montant);

private:
    double m_detteCreance = 0.0f;
};

