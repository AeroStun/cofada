#pragma once
#include <string>
#include <vector>

#include "Personne.h"

class Compte {
public:

	// Constructeur et Destructeur
	Compte() = default;
	Compte(std::string nom, std::string dateDebut, std::string dateFin, int nbParticipants);

	// Accesseurs
	std::size_t getNbParticipants() const;
	double getBudgetTotalDepense() const;
	double getPartIndividuelle() const;
	Personne getParticipant(int indicePrenom) const;
	const std::vector<Personne>& getParticipants() const;

	// Autres methodes
	void ajoutDepenseBudget(int indicePrenom, double nouvelleDepense);

private:
	std::vector<Personne> m_participants;
	std::string m_nom;
	std::string m_dateDebut;		// ../../....
	std::string m_dateFin;		// ../../....
	double m_budgetTotalDepense;
	double m_partIndividuelle;
};
