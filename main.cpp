#include <iostream>
//#include <unistd.h>
//#include <sys/syscall.h>

#include "Compte.h"

int menu1(){
	int choix;

	std::cout << "####  COFADA  ####\n\n";
	std::cout << "[1] Créer un compte\n";
	std::cout << "[2] Consulter un compte\n";
	std::cout << "[3] Crédits\n";
	std::cout << "[4] Quitter\n\n";

	std::cout << "Votre choix : " << std::flush;
	std::cin >> choix;
	while (choix < 1 || choix > 4)
	{
		std::cout << "DEBUG : " << choix << '\n';
		std::cout << "Erreur: mauvaise entrée (1,2,3,4)\n";
		std::cout << "Votre choix : " << std::flush;
		std::cin >> choix;
	}
	std::cout << std::endl;
	return choix;
}

int menu2(){
	//syscall(SYS_clear);

	int choix;

	std::cout << "# Consultation compte #\n\n";
	std::cout << "[1] Ajouter une dépense\n";
	std::cout << "[2] Consulter les dépenses\n";
	std::cout << "[3] Consulter les dettes/créances\n";
	std::cout << "[4] Retour\n\n";

	std::cout << "Votre choix : " << std::flush;
	std::cin >> choix;
	while (choix < 1 || choix > 4){
		std::cout << "DEBUG : " << choix << '\n';
		std::cout << "Erreur: mauvaise entrée (1,2,3,4)\n";
		std::cout << "Votre choix : " << std::flush;
		std::cin >> choix;
	}
	std::cout << std::endl;
	return choix;
}

int choixPersonne(Compte c){
	int choix;

	for(auto i = 0u; i < c.getNbParticipants();)
		std::cout << '[' << ++i << "] " << c.getParticipant(i).getPrenom() << '\n';

	std::cout << "Votre choix : " << std::flush;
	std::cin >> choix;
	while (choix < 1 || choix > c.getNbParticipants()){
		std::cout << "DEBUG : " << choix << '\n';
		std::cout << "Erreur: mauvaise entrée\n";
		std::cout << "Votre choix : " << std::flush;
		std::cin >> choix;
	}
	std::cout << std::endl;
	return choix;
}

int main() {

	int position = -1;		// 0 quitter # 1 menu1 # 2 menu2

	Compte cpt1("Ski 2017", "04/02/2017", "11/02/2017", 3);

	std::cout << cpt1.getNbParticipants() << '\n';
	std::cout << cpt1.getBudgetTotalDepense() << '\n';
	std::cout << cpt1.getPartIndividuelle() << '\n';

	position = 2;

	do {
		switch (menu2()){
			case 1:{
				std::cout << "Choisissez le numéro de la persone ayant fait la dépense.\n";
				int pers = choixPersonne(cpt1);
				std::cout << "Montant payé : ";
				double montant;
				std::cin >> montant;
				cpt1.ajoutDepenseBudget(pers-1, montant);

				std::cout << cpt1.getParticipant(pers-1).getPrenom() << " a payé : " << montant << ".\nMise à jour du compte.\n";
					std::cout << "Budget total dépensé : " << cpt1.getBudgetTotalDepense() << '\n';
					std::cout << "Part individuelle : " << cpt1.getPartIndividuelle() << std::endl;
					
			}break;
			case 2:{
				std::cout << "# Dépenses totales de tous les participants #\n\n";
				std::cout << "Budget total : " << cpt1.getBudgetTotalDepense() << "\n\n";
				for(const auto& p : cpt1.getParticipants())
					std::cout << p.getPrenom() << " a dépensé " << p.getDepensesTotales() << '\n';
				std::cout.flush();
			}break;
			case 3:{
				std::cout << "# Dettes et créances - Synthèse #\n\n\t";
				// Affichage ligne d'en-tête
				for(const auto& p : cpt1.getParticipants()){
					std::cout << p.getPrenom() << "\t\t";
				}
				std::cout << "Total\n";
				// Affichage des dettes par participants
				for(const auto& p : cpt1.getParticipants()){
					std::cout << p.getPrenom() << "\t";
					for(auto j = 0u; j < cpt1.getNbParticipants(); ++j){
							std::cout << p.getDetteCreance(j).getDetteCreance() << "\t\t";
							// Here as well, the double loop seems to be messy-ish
						}
						std::cout << "\t\t" << p.getOperationsBTotales() << '\n';
					}
				std::cout.flush();
			}break;
			case 4:
				position = 0;
				break;
			default:
				break;
		}
	} while(position != 0);

	return 0;
}
