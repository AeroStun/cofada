#pragma once
#include <string>
#include <vector>

#include "OperationB.h"

class Personne {
public:

	// Constructeur et destructeur
	Personne() = default;
	Personne(std::string prenom, int nbParticipants);

	// Accesseurs
	std::string getPrenom() const;
	double getDepensesTotales() const;
	void setDetteCreance(int indicePrenom, double montant);
	OperationB getDetteCreance(int indicePrenom) const;
	double getOperationsBTotales() const;

	// Autres methodes
	void ajoutDepense(double montantDepense);
	void calculOperationsBTotales();

	private:
	std::string m_prenom;
	double m_depensesTotales;				// engagées par la personne
	double m_operationsBTotales;			// somme des dettes et créances
	std::vector<OperationB> m_dettesCreances;	// 1 par participant
};
